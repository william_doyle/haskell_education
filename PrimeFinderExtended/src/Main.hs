-- find primes between 0 and the user specified number
-- AND DO IT AGAIN BUT FASTER
-- william doyle
-- September 7th and 8th 2021

module Main where
import Control.Monad -- allows use of `when`
import Data.Fixed
import Prelude
import Data.Time
import Data.Time.Clock.POSIX
import Data.Int
import Control.Exception
import System.CPUTime
import Text.Printf
import System.Environment

recursivePrimeFinderPure :: Integer -> Integer -> [Integer] -> [Integer]
isPrime :: Integer -> Bool
loopAndMod :: Integer -> Integer -> Bool
eratosthenes :: [Integer] -> [Integer]
dropMultiples :: [Integer] -> Integer -> [Integer]

loopAndMod n i =  
  if i < n
    then 
       if n `mod` i == 0
         then
           False
         else do
           loopAndMod n  (i + 1)
    else
      True 

isPrime n = 
  if n < 2  
    then 
      False
    else  
       loopAndMod n 2 
      
recursivePrimeFinderPure index limit found = 
  if index < limit
    then
      if isPrime index  
        then
          recursivePrimeFinderPure (index + 2) limit (found ++ [index])
        else
          recursivePrimeFinderPure (index + 2) limit found
    else
      found

dropMultiples arr base = 
  if not(null arr)
    then 
      if head arr `mod` base <= 0
        then 
          dropMultiples (tail arr) base
        else
          head arr : dropMultiples (tail arr) base
    else
      arr
 
--  https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes
eratosthenes numbers = 
  if not(null numbers)
    then do 
      let filtered = dropMultiples (tail numbers) (head numbers)
      head numbers : eratosthenes filtered
    else
      numbers

main :: IO ()
main = do 
  putStrLn "Enter upper bound of primes"
  sNum <- getLine
  putStrLn "----The Primes Are----"
  let num = read sNum :: Integer
  let startList = [2]::[Integer]
  start <- getCPUTime
  let arr = recursivePrimeFinderPure 3 num startList
  print arr -- because haskell is lazy we need to use this value to ensure its actually calculated 
  end <- getCPUTime
  let diff = (fromIntegral (end - start)) / (10^12)
  printf "Computation time: %0.5f sec\n" (diff :: Double)
  putStrLn "----------------------"
  --  C O D E   F O R   F A S T   V E R S I O N
  putStrLn "----Faster!-----------"
  start2 <- getCPUTime
  print (eratosthenes [2, 3 .. num])
  end2 <- getCPUTime
  let diff2 = (fromIntegral (end2 - start2)) / (10^12)
  printf "Computation time: %0.5f sec\n" (diff2 :: Double)
  putStrLn "----------------------"
