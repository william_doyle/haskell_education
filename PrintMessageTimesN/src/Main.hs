module Main where
import Control.Monad -- allows use of `when`

-- declare a function with 
--   name: recursivePrintJob
--   type: IO()
--   args: [string, integer, integer]
recursivePrintJob :: String -> Integer -> Integer -> IO()

-- define the function
recursivePrintJob msg index goal = 
  when (index < goal)
      $ putStrLn (show (index + 1) ++ ". " ++ msg)
      >>
      recursivePrintJob msg (index + 1) goal

main :: IO ()
main = do
  putStrLn "Enter message: "
  message <- getLine                                                            -- get users message
  putStrLn "Enter number of times to print: "
  sNumberPrintJobs <- getLine                                                   -- get number from user as string
  putStrLn ("Printing \"" ++ message ++ "\" " ++ sNumberPrintJobs ++ " times.")
  let numberPrintJobs = read sNumberPrintJobs :: Integer                        -- Integer from string
  recursivePrintJob message 0 numberPrintJobs                                   -- start recursive output calls
