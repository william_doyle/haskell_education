module Main where

recursivePrintJob :: String -> Integer -> Integer -> IO()
recursivePrintJob msg index goal = do
  if index < goal
    then
      -- print then call self with less index
      putStrLn (show (index) ++ ". " ++ msg)
      >>
      recursivePrintJob msg (index + 1) goal
    else 
      return ()

main :: IO ()
main = do
  putStrLn "Enter message: "
  message <- getLine
  putStrLn "Enter number of times to print: "
  sNumberPrintJobs <- getLine
  putStrLn ("Printing " ++ message ++ " " ++ sNumberPrintJobs ++ " times.")
  let numberPrintJobs = read sNumberPrintJobs :: Integer
  recursivePrintJob message 0 numberPrintJobs
