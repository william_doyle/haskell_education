module Main where

import Control.Monad -- allows use of `when`
import Numeric -- (showIntAtBase)
import Data.Char -- (intToDigit)
import qualified Data.Text as Text

calculateCell :: String -> String -> String
calculateCell pState rule = 
  case pState of 
    "111" -> [head rule]
    "110" -> [rule !! 1]
    "101" -> [rule !! 2]
    "100" -> [rule !! 3]
    "011" -> [rule !! 4]
    "010" -> [rule !! 5]
    "001" -> [rule !! 6]
    "000" -> [rule !! 7]

padZeros :: String -> Int -> String
padZeros rval targetDepth 
  |  targetDepth < 1 = ""
  |  length rval == targetDepth = rval
  |  otherwise = padZeros (rval ++ "0") targetDepth

binaryString :: Integer -> String 
binaryString x = do 
  let bs = showIntAtBase 2 intToDigit x ""
  if length bs >= 8 then bs
    else do 
      -- pad the string with zeros
      let missingZeros = 8 - length bs
      padZeros "" missingZeros ++ bs 

ensureLengthThree :: String -> String -> String 
ensureLengthThree s making
 | length s == 3 = s
 | null making = "0" ++ s
 | otherwise = s ++ padZeros "" (3 - length s)

padGen :: String -> Int -> String 
padGen gen padTo = do 
  let missingZeros = padTo - (length gen `quot` 2)
  let zeros = padZeros "" missingZeros
  zeros ++ gen ++ zeros 

generateLine :: String -> String -> String -> Int -> Int -> String 
generateLine prev rule making limit initialConditionLength = 
  if length making == length prev 
    then 
      padGen making (limit + initialConditionLength)
    else do 
      -- get substring of previous state
      let substr = drop (length making - 1) (take ((length making - 1)+3) prev)
      -- make sure its 3 chars long -- pad if needed 
      let psubstr = ensureLengthThree substr making
      -- grow 'making' by adding the result of 'calculate cell'
      let newmaking = making ++ (calculateCell psubstr rule)
      -- recursivly call generateLine
      generateLine prev rule newmaking limit initialConditionLength

generate :: String -> String -> Int -> Int -> String -> Int -> String 
generate prev rule count limit state initialConditionLength = 
  if count >= limit 
    then state
    else do
      let thisLine = generateLine prev rule "" limit initialConditionLength
      generate thisLine rule (count + 1) limit (state ++ "\n" ++ thisLine) initialConditionLength

stringReplace :: String -> String -> String -> String 
stringReplace body old new = Text.unpack (Text.replace (Text.pack old) (Text.pack new) (Text.pack body))

numOrDefault :: String -> Integer
numOrDefault n = if null n then 30
  else read n :: Integer

initialConditionsOrDefault :: String -> String
initialConditionsOrDefault cond = if null cond then "010"
  else cond

numLinesOrDefault :: String -> Integer
numLinesOrDefault n = if null n then 12
  else read n :: Integer

inputYes :: String -> Bool
inputYes input 
  | null input = False
  | head input == 'y' = True
  | head input == 'Y' = True
  | otherwise = False

main :: IO ()
main = do
  -- GET RULE FROM KEYBOARD
  putStrLn "Enter Rule:"
  sRule <- getLine
  let rule = numOrDefault sRule
  
  -- GET INITIAL CONDITIONS FROM KEYBOARD
  putStrLn "Enter Initial Conditions:"
  incon <- getLine
  let tempInitialConditions = initialConditionsOrDefault incon -- initial conditions before padding
  let initialLength = length tempInitialConditions 
 
  -- GET NUMBER OF ITERATIONS (LINES) FROM KEYBOARD
  putStrLn "Enter Number Of Generations:"
  slines <- getLine
  let nlines = fromIntegral (numLinesOrDefault slines ) 

  let initialConditions = padGen tempInitialConditions (nlines + initialLength) 

  putStrLn("Rule " ++ show rule ++ " is \"" ++ binaryString rule ++ "\"")
  
  let lines = generate initialConditions (binaryString rule) 0 nlines initialConditions initialLength

  -- ASK USER IF THEY WANT CONSOLE OUTPUT
  putStrLn "Console Output? (y/n)"
  consoleOut <- getLine
  -- ASK USER IF THEY WANT TXT FILE OUTPUT
  putStrLn "Text File Output? (y/n)"
  txtFileOut <- getLine
  let l2 = stringReplace ( stringReplace lines "0" " ") "1" "█"
 
  let fprefix = "output/r_" ++ show rule ++ "_gen_" ++ slines ++ "_incond_" ++ tempInitialConditions 
   
  when (inputYes consoleOut)
    $ putStrLn l2
 
  when (inputYes txtFileOut)
    $ writeFile (fprefix ++ ".txt") l2

  -- WRITE TO FILE SYSTEM AS IMAGE
  -- PBM --> http://netpbm.sourceforge.net/doc/pbm.html
  let pbmText = "P1\n" ++ show (length initialConditions) ++ " " ++ show nlines ++ "\n" ++ lines
  
  -- ASK USER IF THEY WANT IMAGE OUTPUT
  putStrLn "Image Output? (y/n)"
  imageOut <- getLine

  when (inputYes imageOut)
    $ writeFile (fprefix ++ ".pbm") pbmText
