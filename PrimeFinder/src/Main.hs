-- find primes between 0 and the user specified number
-- william doyle
-- September 7th 2021

module Main where
import Control.Monad -- allows use of `when`
import Data.Fixed
import Prelude

recursivePrimeFinder :: Integer -> Integer -> IO()
isPrime :: Integer -> Bool
loopAndMod :: Integer -> Integer -> Bool

loopAndMod n i =  
  if i < n
    then 
       if n `mod` i == 0
         then
           False
         else do
           loopAndMod n  (i + 1)
    else
      True 

isPrime n = 
  if n < 2  
    then 
      False
    else  
       loopAndMod n 2 
      
recursivePrimeFinder index limit = 
  when (index < limit)
    $ if isPrime index  
        then
          print index
          >>
          recursivePrimeFinder (index + 1) limit
        else
          recursivePrimeFinder (index + 1) limit

main :: IO ()
main = do 
  putStrLn "Enter upper bound of primes"
  sNum <- getLine
  putStrLn "----The Primes Are----"
  let num = read sNum :: Integer
  recursivePrimeFinder 0 num
  putStrLn "----------------------"
