{-# LANGUAGE CPP #-}
{-# LANGUAGE NoRebindableSyntax #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
module Paths_PrimeFinder (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/wdd/.cabal/bin"
libdir     = "/home/wdd/.cabal/lib/x86_64-linux-ghc-8.6.5/PrimeFinder-0.1.0.0-jyU7gas5sZ12x10aCT8l7-PrimeFinder"
dynlibdir  = "/home/wdd/.cabal/lib/x86_64-linux-ghc-8.6.5"
datadir    = "/home/wdd/.cabal/share/x86_64-linux-ghc-8.6.5/PrimeFinder-0.1.0.0"
libexecdir = "/home/wdd/.cabal/libexec/x86_64-linux-ghc-8.6.5/PrimeFinder-0.1.0.0"
sysconfdir = "/home/wdd/.cabal/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "PrimeFinder_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "PrimeFinder_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "PrimeFinder_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "PrimeFinder_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "PrimeFinder_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "PrimeFinder_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
