module Main where

import Data.Complex
import Data.Time
import Data.Time.Clock.POSIX
import System.CPUTime
import Text.Printf
import Control.Parallel

makeComplex :: Double -> Double -> Complex Double 
makeComplex x y = x:+y

getIterationCount :: Integer
getIterationCount = 1000

getExponent :: Integer
getExponent = 2

resolution :: Double
resolution = 0.01

test :: Complex Double -> Bool
test cmpx 
  | imagPart cmpx > 2 = False
  | imagPart cmpx < (-2.0) = False
  | realPart cmpx > 2.0 = False
  | realPart cmpx < (-2.0) = False
  | otherwise = True

stringFromBinaryFloor :: Integer -> String -> String -> String
stringFromBinaryFloor n zeroSuplement oneSuplement 
  | n == 0 = zeroSuplement
  | otherwise = oneSuplement
  
rowToString :: [Integer] -> String -> String -> String -> IO String
rowToString row s zeroSuplement oneSuplement =
  if length row * 2 <= length s
    then return (s ++ "\n")
    else do
      let el =  row !! (length s `div` 2)
      let nextChar = stringFromBinaryFloor el zeroSuplement oneSuplement 
      rowToString row (s ++ nextChar) zeroSuplement oneSuplement
  
mapToString :: [[Integer]] -> String -> Integer -> String -> String -> IO String
mapToString map s i zeroSuplement oneSuplement = 
  if length map <= fromIntegral i
    then return  s
    else do
      nextRow <- rowToString (map !! fromIntegral i) " " zeroSuplement oneSuplement 
      let nextS = s ++ nextRow
      mapToString map nextS (i + 1) zeroSuplement oneSuplement 

mapToStringForPBM ::  [[Integer]] -> String -> Integer -> IO String
mapToStringForPBM map s i = mapToString map s i "0 " "1 "

mandelbrot :: Complex Double -> Integer -> Integer -> Complex Double -> Integer
mandelbrot current depth maxdepth cmpx = 
  if depth < maxdepth
    then do 
      let nextCurrent = (current ^ 2) + cmpx
      if test nextCurrent
        then 
          mandelbrot nextCurrent (depth + 1) maxdepth cmpx 
        else
          1
    else
      0

startMandelbrot :: Complex Double -> Integer
startMandelbrot = mandelbrot (makeComplex 0.0 0.0) 0 100

makeRow :: Double -> [Integer]
makeRow rcomp = do 
  let icomps = [-2.0, (-2.0 + resolution) .. 2.0]
  let curriedMakeComplex = makeComplex rcomp
  let unprocessed = map curriedMakeComplex icomps 
  map startMandelbrot unprocessed

--parMakeRow :: Double -> [Integer]
--parMakeRow rcomp = par (makeRow rcomp) 


main :: IO ()
main = do
  start <- getCPUTime
  --let theMap = map parMakeRow [-2.0, (-2.0 + resolution) .. 2.0]
  let pmakeRow = par makeRow
  --let theMap = map pmakeRow [-2.0, (-2.0 + resolution) .. 2.0]
  let theMap = map makeRow [-2.0, (-2.0 + resolution) .. 2.0]
  bitmap <- mapToStringForPBM theMap "" 0
  let x = floor((1/resolution) * 4) +1
  let pbmHeader = "P1\n" ++ show x ++ " " ++ show x ++ "\n"
  let image = pbmHeader ++ bitmap
  let fname = "sets/mandelbrot_res_" ++ show resolution ++ "_iter_" ++ show getIterationCount++ "_exp_" ++ show getExponent ++ ".pbm"
  putStrLn ("info: saving " ++ fname)
  writeFile fname image 
  end <- getCPUTime
  let diff = (fromIntegral (end - start)) / (10^12)
  printf "Computation time: %0.5f sec\n" (diff :: Double)
