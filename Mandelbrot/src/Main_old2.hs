module Main where

import Data.Complex
import Control.Monad -- allows use of `when`
import Data.Fixed
import Prelude
import Data.Time
import Data.Time.Clock.POSIX
import Data.Int
import Control.Exception
import System.CPUTime
import Text.Printf
import System.Environment

import Control.Concurrent
import Control.Concurrent.Async

makeComplex :: Double -> Double -> Complex Double 
--makeRow :: Double -> Double -> Double -> Double -> [Integer] -> IO [Integer]
makeRow :: Double -> Double -> Double -> Double -> [Integer] -> [Integer]
--for :: Double -> Double -> Double -> [[Integer]] -> IO [[Integer]]
test :: Complex Double -> Bool
--for :: Double -> Double -> Double -> [[Integer]] -> [[Integer]]
foo :: Complex Double -> Integer
apply :: Complex Double -> Complex Double -> Integer -> Integer 
main :: IO ()

makeComplex x y = x:+y

getIterationCount = 1000
getExponent = 2
stepSize = 0.01

-- values for resolution which give strange images
-- 0.03
-- 0.06
-- 0.07
-- 0.09
-- 0.011
-- 0.012
-- 0.013

test cmpx 
  | imagPart cmpx > 2 = False
  | imagPart cmpx < (-2.0) = False
  | realPart cmpx > 2.0 = False
  | realPart cmpx < (-2.0) = False
  | otherwise = True

apply cmpx cur i = 
  if i < getIterationCount 
   then do 
     -- do work
     let ncur = (cur ^ getExponent) + cmpx -- normal mandelbrot
     --let ncur = (cur ^ 2) + cmpx ^ 2 -- something else 
     if test ncur 
       then 
         --apply ncur cur (i + 1) 
         apply cmpx ncur (i + 1) 
       else
         getIterationCount - i
   else
     0 

foo cmpx = do 
  let cur = makeComplex 0.0 0.0
  apply cmpx cur 0 
  

makeRow i k max increm arr = do
  let cmpx = makeComplex i k
  let tmp = foo cmpx  -- apply function
  if k <= max
    then --do 
     -- putStrLn ("\trecursive call to makeRow [i, k] is " ++ show i ++ " " ++ show k)
      makeRow i (k + increm) max increm (arr ++ [tmp])
    else do
      let rval = arr ++ [tmp]
      --putStrLn ("\tGiving from makeRow... row is " ++ show rval)
      --return rval
      rval

stringFromBinaryFloor :: Integer -> String -> String -> String
stringFromBinaryFloor n zeroSuplement oneSuplement 
  | n == 0 = zeroSuplement
  | otherwise = oneSuplement
  
rowToString :: [Integer] -> String -> String -> String -> IO String
rowToString row s zeroSuplement oneSuplement  = do 
  if (length row )* 2 <= length s
    then return (s ++ "\n")
    else do
      let el =  row !! (length s `div` 2)
      let nextChar = stringFromBinaryFloor el zeroSuplement oneSuplement 
      t <- (rowToString row (s ++ nextChar) zeroSuplement oneSuplement)
      return t


  
mapToString :: [[Integer]] -> String -> Integer -> String -> String -> IO String
mapToString map s i zeroSuplement oneSuplement = 
  if length map <= fromIntegral i
    then return  s
    else do
      --let nextRow = rowToString (map !! fromIntegral i) " " zeroSuplement oneSuplement 
      nextRow <- rowToString (map !! fromIntegral i) " " zeroSuplement oneSuplement 
      let nextS = s ++ nextRow
      t <-mapToString map nextS (i + 1) zeroSuplement oneSuplement 
      return t

mapToStringForPBM ::  [[Integer]] -> String -> Integer -> IO String
mapToStringForPBM map s i = mapToString map s i "0 " "1 "

mapToStringForTerminal ::  [[Integer]] -> String -> Integer -> IO String
mapToStringForTerminal map s i = mapToString map s i " " "█"

for :: Double -> Double -> Double -> [[Integer]] -> [[Integer]]
for i max increm mapSoFar = do 
  let row = makeRow i (-2.0) 2.0 increm [] 
  let nextMapVersion = mapSoFar ++ [row] 
  if i < max 
    then 
      for (i + increm) max increm nextMapVersion
    else
      nextMapVersion

forThreaded :: Double -> Double -> Double -> [[Integer]] -> IO [[Integer]]
forThreaded i max increm mapSoFar = do 
  let complexComponents = [-2.0, (-2.0 + increm) .. 2.0]
--  let indexFromReal = \real -> (real + 2.0) / increm 
  --let tmps = map indexFromReal complexComponents

  let rows = map (\real -> makeRow ((\real -> (real + 2.0) /increm)real) real 2.0 increm []) complexComponents 
  
  print rows
  return rows


 
main = do

  start <- getCPUTime

  theMap <- forThreaded (-2.0) 2.0 stepSize []
  bitmap <- mapToStringForPBM theMap "" 0
  
  let x = floor((1/stepSize) * 4) +1

--  print x


  let pbmHeader = "P1\n" ++ show x ++ " " ++ show x ++ "\n"
  let image = pbmHeader ++ bitmap
  let fname = "sets/mandelbrot_res_" ++ show stepSize ++ "_iter_" ++ show getIterationCount++ "_exp_" ++ show getExponent ++ ".pbm"
  putStrLn ("info: saving " ++ fname)
  writeFile fname image 

  end <- getCPUTime
  let diff = (fromIntegral (end - start)) / (10^12)
  putStrLn ("[" ++ show diff ++ " seconds]")
  
